import { createApp } from "vue";
import App from "./App.vue";
import Router from "./router";
import ElementPlus from "element-plus";
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import "./index.scss";

const app = createApp(App);

app.use(ElementPlus);
app.use(Router);


for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

app.mount("#app");
