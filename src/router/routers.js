import Main from "../components/main";
export default [
  {
    path: "/",
    name: "/",
    component: Main,
    meta: {
      title: "首页",
    },
    redirect: "/home",
    children: [{
      name: "home",
      path: "/home",
      meta: {
        title: "首页",
      },
      component: () => import("../views/home/index.vue")
    }]
  },
  {
    path: "/about",
    name: "about",
    component: Main,
    meta: {
      title: "关于我们",
    },
    children: [{
      path: "/index",
      meta: {
        title: "关于我们",
      },
      name: "about_index",
      component: () => import("../views/about/index.vue")
    }]
  }
];