import Routers from "./routers";
import { createRouter, createWebHistory } from "vue-router";

export default createRouter({
  routes: Routers,
  history: createWebHistory()
})