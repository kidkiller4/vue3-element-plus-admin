const path = require("path");
const { defineConfig } = require('@vue/cli-service')

const resolve = dir => {
  return path.join(__dirname, dir)
}

module.exports = defineConfig({
  transpileDependencies: true,
  pluginOptions:{
    "style-resources-loader":{
      preProcessor:"scss",
      patterns: [resolve("./src/assets/style/variable.scss")]
    }
  },
})
